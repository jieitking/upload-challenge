if [[ $2 == "1" ]]
  then
    while read -r line
    do
    if [[ "${line}" != "#"* ]] && [[ "${#line}" > 1 ]]
      then
        line=$(echo ${line} | sed 's/\\n/\\\\n/g')
        if [[ -z $txt ]]
          then
            txt="ENV ${line}"
          else
            txt="${txt}\nENV ${line}"
        fi
    fi
    done < ./env/dev.env
    gsed -i "/#environment variable here/a $txt" Dockerfile
  elif [[ $2 == "0" ]]
    then
      gsed -i '/^ENV /d' Dockerfile
fi
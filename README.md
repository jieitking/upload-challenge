# README #

Service-rocket technical test - upload challenge
total-time to complete -> 6+hours + 30 mins for depployment, readme and minor bug fix

### What is done ###
* Story 1 , Story 2.1, Story 2.2
* Set up the configuration of Jest for testing (eg. GlobaSetup...)
* Docker setup
* Deploy to AWS

### What to expect ###
* You can upload 5 images and zip file at the same time

### What is not done ###
* Integration and Unit Testing

### What to take note ###
* This repo does not have aws access key, if you run in local there will be no file upload to s3, you are free to add urs in "./env/dev.env"
* Files in tmp will be deleted after files is uploaded to s3
* In case you want to test the deploy script(basically the docker part), remember to grant the permission for deploy.sh and merge-env.sh with "chmod +x"

### Aws ###
* This application is deploy to AWS with Fargate
* Access key is not include, same as cloudformation Yaml file
* Please let me know once you no longer need this app on cloud, so i can terminate the service to save cost
* Please expect slow response as the server is lowest spec
* API url -> http://rocke-Publi-S6JG1X6U1H6W-245704919.ap-southeast-1.elb.amazonaws.com/upload

### API ###
* Post and form-data(Multipart)
* key: image -> image type (jpeg/png)  "Maximum 5 images"
* key: zip -> zip type (zip)   "Maximum 1 file"
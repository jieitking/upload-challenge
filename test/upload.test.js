const request = require('supertest')
const {app} = require('../src/server')
import path from 'path'
import Jimp from 'jimp'

jest.setTimeout(30000)

describe('Get Endpoints', () => {
  it('should return Server health check ok!', async () => {
    const res = await request(app).get('/healthcheck').send()

    expect(res.text).toEqual('Server health check ok!')
  })
})


describe('Upload Image', () => {
    it('should upload image successfully and thumbnails is generated', async () => {
        let thumbnails64 = 0, thumbnails32 = 0
        const imageRes = await request(app)
                        .post('/upload')
                        .attach('image', path.resolve(__dirname, './tmp/pic1.jpeg'))
                        .expect(200)                
        
        for (const image of imageRes.body) {
            const imageSize = await Jimp.read(image)
            if(imageSize.bitmap.height === 64)
                thumbnails64 += 1
            else if(imageSize.bitmap.height === 32)
                thumbnails32 += 1
        }

        expect(imageRes.body.length === 3)
        expect(thumbnails64 === 1)
        expect(thumbnails32 === 1)
    })
})


describe('Upload Zipped Images', () => {
    it('should upload zipped images successfully and thumbnails is generated', async () => {
        let thumbnails64 = 0, thumbnails32 = 0
        const imageRes = await request(app)
                        .post('/upload')
                        .attach('zip', path.resolve(__dirname, './tmp/zipped.zip'))
                        .expect(200)
        
        for (const image of imageRes.body) {
            const imageSize = await Jimp.read(image)
            if(imageSize.bitmap.height === 64)
                thumbnails64 += 1
            else if(imageSize.bitmap.height === 32)
                thumbnails32 += 1
        }

        expect(imageRes.body.length === 12)
        expect(thumbnails64 === 4)
        expect(thumbnails32 === 4)
    })

})


describe('Upload File with wrong format', () => {
    it('should fail to upload', async () => {
        const res = await request(app)
                        .post('/upload')
                        .attach('image', path.resolve(__dirname, './tmp/zipped.zip'))
                        .expect(422)
    })
})
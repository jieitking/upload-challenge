import fs from 'fs-extra'

// remove all the files from tmp
module.exports = async() => {
    const fileList = await fs.readdirSync(`./tmp`)
    for (const file of fileList) {
        if(file !== "README.md")
            await fs.removeSync(`./tmp/${file}`)
    }
}
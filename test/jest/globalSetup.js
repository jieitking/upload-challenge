import "core-js/stable"
import { app } from '../../src/server'

const port = 7777
module.exports = async () => {
  await app.listen({
      port,
    }, async () => {  
      console.log(`Server ready at http://localhost:${port}`)
  })
}
import multer from 'multer'
import moment from 'moment-timezone'
import {validateUploadType} from '../utils/mimeTypeValidation'

//declare upload config
const upload = multer({
    fileFilter: async(req, file, cb) => {
        if (!validateUploadType(file))
            return cb(new Error('File type is not supported!'))
        cb(null, true)
    },
    limits: { fileSize: 10 * 1024 * 1024 }, //10mb
    storage: multer.diskStorage({
        destination: async(req, file, cb) => 
            cb(null, './tmp'),
        filename: async(req, file, cb) => 
            cb(null,`${file.fieldname}-${moment().tz('Asia/Singapore').format('x.')}${file.mimetype.substr(file.mimetype.lastIndexOf("/") + 1)}`)
    })
})

//maximum upload 5 images, 1 zip file at the same time
const multiUpload = upload.fields([
    { name: 'zip', maxCount: 1 },
    { name: 'image', maxCount: 5 }
])

module.exports = multiUpload
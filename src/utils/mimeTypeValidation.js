import { supportImageType, supportZipType } from './constant'

/**
 * check type of upload file
 * @param {String} mimetype 
 */
const validateUploadType = ({fieldname, mimetype}) => {
    if(fieldname === 'image')
        return supportImageType.includes(mimetype)
    else if(fieldname === 'zip')
        return supportZipType.includes(mimetype)
    
    return false
}

/**
 * check type of image
 * @param {String} mimetype 
 */
const validateImageType = (mimetype) => supportImageType.includes(mimetype)

export {
    validateUploadType,
    validateImageType
}
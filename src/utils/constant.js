/**
 * Constant variables
 */

const supportImageType = ['image/jpeg', 'image/png']
const supportZipType = ['application/zip']

export {
    supportImageType,
    supportZipType
}
import express from 'express'
import _ from 'lodash'
import multiUpload from '../middleware/multer'
import AdmZip from 'adm-zip'
import FileType from 'file-type'
import {validateImageType} from '../utils/mimetypeValidation'
import fs from 'fs-extra'
import { v4 as uuidv4 } from 'uuid'
import {uploadLocalMedia} from '../services/awsS3'
import Jimp from 'jimp'
import moment from 'moment-timezone'

const router = express.Router()

router.post('/', async (req, res) => {
    multiUpload(req, res, async (err) => {
        //handle error
        if (err)
            return res.status(422).send({ errors: [{ title: 'Upload Error', detail: (err.message) ?err.message :err}] })

        const dirName = await uuidv4()
        
        //extra zip files
        if(_.has(req, 'files.zip')) {
            const zip = new AdmZip(`./tmp/${req.files.zip[0].filename}`)
            const zipEntries = zip.getEntries()
    
            for (const zipEntry of zipEntries) {
                const ext = await FileType.fromBuffer(zipEntry.getData())
                if(!validateImageType(ext.mime)) 
                    return res.status(422).send({ errors: [{ title: 'Upload Error', detail: 'Wrong file type in zip'}] })                
            }
            //extract files to folder
            await zip.extractAllTo(`./tmp/${dirName}`, false)
            //remove zip folder
            await fs.removeSync(`./tmp/${req.files.zip[0].filename}`)
        }

        //move image to extra folder
        if(_.has(req, 'files.image')) {
            for (const image of req.files.image)
                await fs.moveSync(`./tmp/${image.filename}`, `./tmp/${dirName}/${image.filename}`, { overwrite: true })
        }

        //list all the file in the folder
        const fileList = await fs.readdirSync(`./tmp/${dirName}`).map(img => `./tmp/${dirName}/${img}`) 
        let uploadArr = []
        
        //generate thumbnails and create upload promise
        for (const file of fileList) {
            const ext = await FileType.fromFile(file)
            const fileName =  moment().tz('Asia/Singapore').format('x')

            uploadArr.push(uploadLocalMedia(fs.readFileSync(file), ext.ext, fileName))

            const fileSize = await Jimp.read(file)
            const ratio = fileSize.bitmap.width / fileSize.bitmap.height
            if(fileSize.bitmap.width >= 128 && fileSize.bitmap.height >= 128) {
                fileSize.resize(64*ratio,64).quality(60).write(`./tmp/${dirName}/thumbnails64-${fileName}.png`)
                fileSize.resize(32*ratio,32).quality(60).write(`./tmp/${dirName}/thumbnails32-${fileName}.png`)
                await new Promise(resolve => setTimeout(resolve, 50))
                uploadArr.push(uploadLocalMedia(fs.readFileSync(`./tmp/${dirName}/thumbnails64-${fileName}.png`), 'png', `thumbnails64-${fileName}`))
                uploadArr.push(uploadLocalMedia(fs.readFileSync(`./tmp/${dirName}/thumbnails32-${fileName}.png`), 'png', `thumbnails32-${fileName}`))
            }
        }
        const urls = (await Promise.allSettled(uploadArr)).map(url => url.value)

        await fs.removeSync(`./tmp/${dirName}`)
        res.status(200).send(urls)
    })
})

module.exports = router
import express from 'express'
import cors from 'cors'
import helmet from 'helmet'
import upload from './routes/upload'
import bodyParser from 'body-parser'

const app = express()

app.use(helmet())
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/healthcheck', (req, res) => { res.status(200).send('Server health check ok!') })
app.use('/upload', upload)

export {
    app
}
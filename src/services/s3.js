import aws from 'aws-sdk'
import { v4 as uuidv4 } from 'uuid'
import { URL } from 'url'

const s3 = new aws.S3({
    accessKeyId: process.env.AWS_accessKeyId,
    secretAccessKey: process.env.AWS_secretAccessKey,
    region: 'ap-southeast-1'
})

const uploadLocalMedia = async (body, mediaFormat, title = null) => {
    return new Promise(async (resolve, reject) => {
        const params = {
            Bucket: process.env.AWS_s3Bucket,
            Key: `${title || uuidv4()}.${mediaFormat}`,
            Body: body,
            ACL: 'private',
            CacheControl: "no-cache"
        }

        s3.upload(params, function (err, data) {
            if (err) {
                reject(err)
                throw new Error(`\n*** {upload local media to S3} *** error:\n${err}`)
            }
            const dataUrl = new URL(data.Location)
            dataUrl.hostname = process.env.CDN_url
            // console.log(`Media uploaded successfully. ${dataUrl.href}`)
            resolve(dataUrl.href)
        })
    })
}

export {
    uploadLocalMedia
}